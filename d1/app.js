const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes')


const app = express();
const port = 4000;


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Syntax: mongoose.connect('<connection string>',{middleware})
//avoid futures error when connecting mongodb

mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.ag1bo.mongodb.net/session30?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true

});

let db = mongoose.connection;

//console.error.bind(console) - print errors in the browser and in the terminal.
db.on("error", console.error.bind(console, "Connection Error"));

//if successful, this will be the output.
db.once('open', () => console.log('Connected to the cloud database.'));


//Mongoose Schema Section

app.use('/tasks', taskRoutes)

//port listen
app.listen(port, () => console.log(`Server is running at localhost ${port}`));
